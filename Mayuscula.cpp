#include<stdio.h>
#include<string.h>

//Funcion para pasar la cadena a mayuscula y eliminar signos----------------------------------------------------------------------------------
void funtion_mayus(char array[]){
	
	int lon;
	
	lon=strlen(array);

	for(int i=0;i<lon;i++){
		
		if( array[i]>=97 && array[i]<=122 ){
			array[i]=array[i]-32;
		}		
	}
}
//-------------------------------------------------------------------------------------------------------------------------------------------
//funcion extraer letras-------------------------------------------------------------------------------------------------------------
void funtion_extract_letters(char array[]){
	
	int lon,j=0;
	char array_extract[100];
	lon=strlen(array);
	
	for(int i=0;i<lon;i++){
		
		if( (array[i]>=97 && array[i]<=122) ||  (array[i]>=65 && array[i]<=90) ){
			
			array_extract[j]=array[i];
			j++;
			
		}
					
	}
	array_extract[j]='\0';
	strcpy(array,array_extract);
}
//----------------------------------------------------------------------------------------------------------------------------------------

//funcion para comprobar si la cadena es palindromo---------------------------------------------------------------------------------------
int funtion_palindrome(char array[]){
	
	int lon,palindrome=0,j=0;
	
	lon=strlen(array);
	
	for(int i=lon-1;i>=0;i--){
		
		if(array[j]!=array[i]){
			palindrome=1;
		}
			
		j++;
	}
	
	return palindrome;
	
}
//-------------------------------------------------------------------------------------------------------------------------------------------




int main(){
	char *men[18]={"Iuaqyf-Ofy-qaui",
					"Qa,ixhgli, sdmhyssyhmdsilg hxaq",
					"Og-r-N ub-fxnoafafaonxfbunrgo",
					"Tramuh-j Oyil, znuuoz-K k, zouunzliyojhumart",
					"Zgoleeyjeaxpyyreeo, e Xfbiibfxeoeeryqpxa ejyeelogz",		
					"woiti, o-pp-Oiti, Os",
					"Ulniiaf, d, T uiutdfai, Inlu",
					"Uqgt, Wlmzeoe-Efwyis, Iy-Ioeo, aa-oeoiyisiywf, eeoezmlwtgqu",
					"G, Amtesq, cx-Csset, Mag",
					"Yey Zuik-Fefo-D-Ad, E Ksmuyzeezyumskexadofef kiuzyey",
					"Efjtb-l-euiee ui Ooun Ao Oanuooi-u ee-iuelbtef-E",
					"Xtooiryyviooosaxmegxioyyomxgemxa, Soooivyyriootx",
					"U-Xnww eempweot, Rtbiciruiuri-cib-Trtoe wpm Eewwnxu",
					"Iynpo-Kyaa, I, Y, Iaaykopn Yi",
					"X Aj-Vly hy lvja, x",
					"Yqegyqodqebvcwonci-Uz Aekbeebkeazuicnowcvbeqdoqygeqy",
					"Voz i iaonwmaac Uy Cbhuyzo-Zyuhb Cyucaa, M W-noai-Izov",
					"Ks vptatanp, V-S-K"};
	
	char messages[100]={""},control,length_men;
	
	for(int i=0;i<18;i++){
		
		strcpy (messages, men[i]);           //Realizando la copia
	
		funtion_extract_letters(messages);	//Extrayendo las letras de la cadena
	
		funtion_mayus(messages);	        //Pasando la cadena a mayuscula
	
		control=funtion_palindrome(messages);       //Comprobando si es palindromo
	
		if(control==0){
			printf("Y ");
		}else{
			printf("N ");
		}
			
	}
		
	return 0;	
}
